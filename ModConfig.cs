﻿using System;

namespace TidyFields
{
	public class ModConfig
	{
		public bool place_torches_in_scarecrows { get; set; } = true;
		public bool place_torches_in_sprinklers { get; set; } = true;
		public bool place_sprinklers_on_fences { get; set; } = true;
	}
}